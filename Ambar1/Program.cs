﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.IO;

namespace Ambar
{
    class Program
    {
        public static List<Player> Players = new List<Player>();

        static void Main(string[] args)
        {
            TcpListener listener = new TcpListener(80);
            listener.Start();
            while (true)
            {
                
                TcpClient client = listener.AcceptTcpClient();
                
                StreamReader sr = new StreamReader(client.GetStream());
                StreamWriter sw = new StreamWriter(client.GetStream());
                NetworkStream ns = client.GetStream();
                string request = sr.ReadLine();
                if (request != null)
                if (!request.Contains(".png") && !request.Contains(".jpg") && !request.Contains('?'))
                {
                    string[] req = request.Split('/');
                    string[] final = req[1].Split(' ');

                    sw.WriteLine("HTTP/1.0 200 OK\n");
                    sw.WriteLine(Page.GetHome(final[0]));
                    sw.Flush();
                    client.Close();
                }
                else if (request.Contains('?'))
                { 
                    string[] req = request.Split('/');
                    string[] final = req[1].Split(' ');
                    Page.resolveQuery(final[0],request,sw,client);
                }
                else if (request.Contains(".png") || request.Contains(".jpg"))
                {
                    
                    string[] req = request.Split('/');
                    string[] final = req[1].Split(' ');
                    // Send entire response via NetworkStream
                    ns.Write(Page.GetImage(final[0]), 0, Page.GetImage(final[0]).Length);
                    client.Close();
                }
                //Console.WriteLine(client.GetStream().ToString());
               // NetworkStream ns = client.GetStream();

               
            }
        }
        public static void displayPage(string page,StreamWriter sw, TcpClient client, string request)
        {
            string[] req = request.Split('/');
            string[] final = req[1].Split(' ');
            sw.WriteLine("HTTP/1.0 200 OK\n");
            sw.WriteLine(page);
            sw.Flush();
            client.Close();
        }
    }


}